import processing.serial.*;
Serial mySerial;
PrintWriter output;
String timestamp;

void setup() {
   mySerial = new Serial( this, Serial.list()[2], 9600 );
   timestamp = year() + nf(month(),2) + nf (day(),2) + "-"  + nf(hour(),2) + nf(minute(),2) + nf(second(),2);
   output = createWriter( timestamp + ".txt" );
}
void draw() {
    if (mySerial.available() > 0 ) {
         String value = mySerial.readString();
         if ( value != null ) {
              output.println( value );
         }
    }
}

void keyPressed() {
    output.flush();  // Writes the remaining data to the file
    output.close();  // Finishes the file
    exit();  // Stops the program
}