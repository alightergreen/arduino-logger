#include <SPI.h>
#include <WiFi101.h>

char ssid[] = "yourNetwork";     //  your network SSID (name)
char pass[] = "secretPassword";  // your network password
int status = WL_IDLE_STATUS;     // the Wifi radio's status
long time;

void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue:
    while (true);
  }

  // attempt to connect to Wifi network:
  while ( status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network:
    status = WiFi.begin(ssid, pass);

    // wait 10 seconds for connection:
    delay(10000);
  }

  // you're connected now, so print out the data:
  Serial.print("You're connected to the network");
}

void loop() {
  // check the network connection once every 10 seconds:
  delay(10000);
 time = millis();  
 Serial.print("["); Serial.print(time); Serial.println("]");
 if ( status != WL_CONNECTED) {
  Serial.println("*");}
 else (Serial.println("+"));
 printSignal();
 }

void printSignal() {
  
  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.println(rssi);
}

